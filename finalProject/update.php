<?php

session_cache_limiter('none');
session_start();

	$movieNameError = "";
	$movieGenreError = "";
	$movieRatingError = "";
	$movieTimeError = "";
	$movieDescriptionError = "";
	
function validateAll(){
	global $movieNameError, $validForm, $movieName, $movieDescription, $movieDescriptionError, $movieGenre, $movieGenreError, $movieTime, $movieTimeError, $movieRating, $movieRatingError;
	if($movieName == ""){
		$validForm = false;
		$movieNameError = "Name Required.";
	}
	if(preg_match('/[@#$%^&*+=\\[\]\;\/{}|:<>~\\\\]/', $movieName)){
		$validForm = false;
		$movieNameError = "No Special Characters.";
	}
	if($movieDescription == ""){
		$validForm = false;
		$movieDescriptionError = "Description Required.";
	}
	if(preg_match('/[@#$%^&*+=\\[\]\;\/{}|<>~\\\\]/', $movieDescription)){
		$validForm = false;
		$movieDescriptionError = "No Special Characters.";
	}
	if($movieGenre == ""){
		$validForm = false;
		$movieGenreError = "Genre Required.";
	}
	if(preg_match('[@#$%^&*()+=\\[\]\;\/{}|:<>~\\\\]/', $movieGenre)){
		$validForm = false;
		$movieGenreError = "No Special Characters.";
	}
	if($movieTime == ""){
		$validForm = false;
		$movieTimeError = "Time Required.";
	}
	if(preg_match('/[@#$%^&*()+=\\[\]\;\/{}|:<>~\\\\]/', $movieTime)){
		$validForm = false;
		$movieTimeError = "No Special Characters.";
	}
	if($movieRating == ""){
		$validForm = false;
		$movieRatingError = "Rating Required.";
	}
	if(preg_match('/[@#$%^&*()+=\\[\]\;\/{}|:<>~\\\\]/', $movieRating)){
		$validForm = false;
		$movieRatingError = "No Special Characters.";
	}

}

if(empty($_SESSION['validUser'])){
    $_SESSION['validUser'] = "no";
}

//
//NAVBAR LOGIN//OPTIONS
//

if($_SESSION['validUser'] == "no"){
	//
	//LOGIN DROPDOWN
	//
		$navBarOptions = "        
		<li class='dropdown'>
          <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Login<span class='caret'></span></a>
			<form method='post' name='loginForm' action='login.php' class='dropdown-menu'>
    
    			<p>Username:</p> 
        		<input type='text' class='blackText' name='inUsername' />
        		<p>Password:</p>
        		<input type='password' class='blackText' name='inPassword' />
        		<p><input type='submit' class='blackText' name='login' value='Login' /><input type='reset' class='blackText' name='reset' /></p>
			</form>
        </li>
		
		<li><a href='help.php'>Help</a></li>";
}

else{
	$navBarOptions = "
	
         
         		
            		<li><a href='shop-page-mens.html'>Add Movie</a></li>
            		<li><a href='logout.php'>Logout</a></li>
					<li><a href='help.php'>Help</a></li>
          		
   ";
	}

//
//UPDATE MOVIE
//

if($_SESSION['validUser'] == "yes"){

	include 'connection.php';
	
if(isset($_POST['submit'])){
	
	$movieName = trim($_POST['movie_name']);
	$movieDescription = $_POST['movie_description'];
	$movieGenre = $_POST['movie_genre'];
	$movieRating = $_POST['movie_rating'];
	$movieTime = $_POST['movie_time'];

	
	$validForm = true;
	
	validateAll();
			
		if($validForm){
		
		$movie_name = $_POST['movie_name'];
		$movie_description = $_POST['movie_description'];
		$movie_genre = $_POST['movie_genre'];
		$movie_rating = $_POST['movie_rating'];
		$movie_time = $_POST['movie_time'];
		$movie_id = $_POST['movie_id'];
		
		$sql = "UPDATE movie_table SET " ;
		$sql .= "movie_name= ?, ";
		$sql .= "movie_description=?, ";
		$sql .= "movie_genre=?, ";
		$sql .= "movie_rating=?, ";
		$sql .= "movie_time=? ";
		$sql .= " WHERE (movie_id='$movie_id')";
		
		$query = $connection->prepare($sql);
		
		$query->bind_param('sssss',$movie_name,$movie_description,$movie_genre,$movie_rating,$movie_time);
		
		if ( $query->execute() ){
			//echo $event_name;
			$message = "<h1 class='text-center'>Your movie has been successfully updated.</h1>";
			$message .= "<p class='text-center'>This page will auto-redirect in 5 seconds. If not, click <a href='index.php'>here.</a></p>";
			header( "refresh:5;url=index.php" );
		}	
	}
	
		else{
			$message = "<h1>You have encountered a problem. Please try again.</h1>";
		$updateRecId = $_GET['recId'];	
		
		
		
		$sql = "SELECT movie_id,movie_name,movie_description,movie_genre,movie_rating,movie_time FROM movie_table WHERE movie_id=?";	
		
	
		$query = $connection->prepare($sql);
		
		$query->bind_param("i",$updateRecId);	
	
		if( $query->execute() )	
		{
			$query->bind_result($movie_id,$movie_name,$movie_description,$movie_genre,$movie_rating,$movie_time);
		
			$query->store_result();
			
			$query->fetch();
		}
		else
		{
			$message = "<h1>You have encountered a problem with your update.</h1>";
			$message .= "<h2>" . mysqli_error($connection) . "</h2>" ;			
		}
	
	$connection->close();
	$query->close();
		}
		
		
	}//end post submit
	
	else{
		$updateRecId = $_GET['recId'];	
		
		
		
		$sql = "SELECT movie_id,movie_name,movie_description,movie_genre,movie_rating,movie_time FROM movie_table WHERE movie_id=?";	
		
	
		$query = $connection->prepare($sql);
		
		$query->bind_param("i",$updateRecId);	
	
		if( $query->execute() )	
		{
			$query->bind_result($movie_id,$movie_name,$movie_description,$movie_genre,$movie_rating,$movie_time);
		
			$query->store_result();
			
			$query->fetch();
		}
		else
		{
			$message = "<h1>You have encountered a problem with your update.</h1>";
			$message .= "<h2>" . mysqli_error($connection) . "</h2>" ;			
		}
	
	$connection->close();
	$query->close();
		}
	
}//end valid user
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Update Movie</title>

<!-- Latest compiled and minified CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<link href="css/styles.css" rel="stylesheet" type="text/css">

<!-- jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



</head>

<body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Movie Collection</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
                        <?php echo $navBarOptions; ?>
                                     
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <br>
    <br>
    <br>
<div class="container container-black">
<br>
	<div class="col-sm-12">
    
    <?php if($_SESSION['validUser'] == "yes"){ 
	
		if(isset($_POST["submit"]) && $validForm)
			{
				echo $message;
			}
		else{
	
	?>
    
		<form name="form1" method="post" action="update.php?recId=<?php echo $movie_id; ?>" class="text-center">
  			<p>&nbsp;</p>
  		<p>
    		<label>Movie Name:
      			<input type="text" name="movie_name" id="movie_name" value="<?php echo $movie_name; ?>"><span class="error"><?php echo $movieNameError; ?></span>
    		</label>
  		</p>
  		<p>
        	<label>Movie Genre: 
   				<input type="text" name="movie_genre" id="movie_genre" value="<?php echo $movie_genre; ?>"><span class="error"><?php echo $movieGenreError; ?></span>
            </label>
  		</p>
  		<p>
        	<label>Movie Rating: 
    			<input type="text" id="movie_rating" name="movie_rating" value="<?php echo $movie_rating; ?>"><span class="error"><?php echo $movieRatingError; ?></span>
            </label>
  		</p>
  		<p>
        	<label>Movie Time: 
    			<input type="text" name="movie_time" id="movie_time" value="<?php echo $movie_time; ?>"><span class="error"><?php echo $movieTimeError; ?></span>
            </label>
  		</p>
 
  		<p>
    		<label> Movie Description:<br>
      			<textarea style="width:100%" name="movie_description" id="movie_description" cols="45" rows="5"><?php echo $movie_description; ?></textarea>
    		</label>
    			<span class="error"><?php echo $movieDescriptionError; ?></span>
  		</p>

  				<input type="hidden" name="movie_id" id="movie_id" value="<?php echo $movie_id ?>" />
		<p>
    		<input type="submit" name="submit" id="button" value="Submit">
    		<input type="reset" name="reset" id="button2" value="Reset">
  		</p>
		</form>
        
     <?php }}
	 	else{
	 ?>
     <p class="text-center jumbotron">Please Login</p>
     <?php
		}
	
	 ?>
    </div>
</div>
</body>
</html>