<?php

session_cache_limiter('none');
session_start();

if(empty($_SESSION['validUser'])){
    $_SESSION['validUser'] = "no";
}

//
//NAVBAR LOGIN//OPTIONS
//

if($_SESSION['validUser'] == "no"){
	//
	//LOGIN DROPDOWN
	//
		$navBarOptions = "        
		<li class='dropdown'>
          <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Login<span class='caret'></span></a>
			<form method='post' name='loginForm' action='login.php' class='dropdown-menu'>
    
    			<p>Username:</p> 
        		<input type='text' class='blackText' name='inUsername' />
        		<p>Password:</p>
        		<input type='password' class='blackText' name='inPassword' />
        		<p><input type='submit' class='blackText' name='login' value='Login' /><input type='reset' class='blackText' name='reset' /></p>
			</form>
        </li>
		
		<li><a href='help.php'>Help</a></li>"
		;
}

else{
	$navBarOptions = "
            		<li><a href='addMovie.php'>Add Movie</a></li>
            		<li><a href='logout.php'>Logout</a></li>
					<li><a href='help.php'>Help</a></li>
					";
	}

//
//Showing movie data
//

include 'connection.php';

$sql = "SELECT movie_name, movie_genre, movie_id FROM movie_table";

$res = $connection->query($sql);

$displayMsg= "";

	if($res)
	{
		
		if ($res->num_rows > 0) 
		{
			$displayMsg = "<h1 class='text-center'>Number of Movies: " . $res->num_rows . "</h1>";
			$displayMsg .= "<table class='table table-bordered table-hover'>";
			$displayMsg .= "<tr><th>Movie</th><th>Genre</th><th>Details</th>";
				if($_SESSION['validUser'] == "yes"){
					$displayMsg .= "<th>Options</th></tr>";
				}
			while($row = $res->fetch_assoc()) 
			{
				
				$displayMsg .= "<tr><td>";
				$displayMsg .= $row["movie_name"];
				$displayMsg .= "</td><td>";
				$displayMsg .= $row["movie_genre"];
				$displayMsg .= "</td>";
				$displayMsg .= "<td><a href=details.php?recId=$row[movie_id]>Details</a>";
				
				
				if($_SESSION['validUser'] == "yes"){
					$displayMsg .= "<td><a href=update.php?recId=$row[movie_id]>Update</a> | <a href=deleteMovie.php?recId=$row[movie_id]>Delete</a></td>";
				}
				
				
				$displayMsg .= "</tr>";
			}
			
			$displayMsg .= "</table>";
		} 
		else 
		{
			$displayMsg .= "0 results";
		}		
	}
	else
	{
		
		$displayMsg .= "<h3>Sorry there has been a problem</h3>";
		$displayMsg .= "<p>" . mysqli_error($connection) . "</p>";			
	}
	$connection->close();


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Movie Collection</title>

<!-- Latest compiled and minified CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />


<!-- jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<link href="css/styles.css" rel="stylesheet" type="text/css" />

</head>

<body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Movie Collection</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
                        <?php echo $navBarOptions; ?>
                                     
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <br>
    <br>
    <br>
<div class="container container-black">
<br>
	<div class="col-sm-12">
		<?php echo $displayMsg; ?>
    </div>
</div>
</body>
</html>
