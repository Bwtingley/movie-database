<?php

session_start();

$deleteRecord = $_GET['recId'];

//echo $deleteRecord;

include 'connection.php';

$sql = "DELETE FROM movie_table WHERE movie_id=?";

$query = $connection->prepare($sql);

$query->bind_param("i",$deleteRecord);

	if ( $query->execute() )			
	{
		$message =  "<h1 class='text-center'>Your movie has been successfully removed.</h1>";
		$message .= "<p class='text-center'>This page will auto-redirect in 5 seconds. If not, click <a href='index.php'>here.</a></p>";
		header( "refresh:5;url=index.php" );	
	}
	else
	{
		$message = "<h1>You have encountered a problem with your removal.</h1>";
		$message .= "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
	}
	$query->close();
	$connection->close();

?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Add Movie</title>

<!-- Latest compiled and minified CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<link href="css/styles.css" rel="stylesheet" type="text/css">

<!-- jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



</head>

<body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Movie Collection</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
                        <?php echo $navBarOptions; ?>
                                     
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <br>
    <br>
    <br>
<div class="container container-black">
<br>
	<div class="col-sm-12">

<h3><?php echo $message; ?></h3>
    </div>
</div>
</body>
</html>