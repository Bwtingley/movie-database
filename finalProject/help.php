<?php
include("Email.php");

session_cache_limiter('none');
session_start();

if(empty($_SESSION['validUser'])){
    $_SESSION['validUser'] = "no";
}

//
//NAVBAR LOGIN//OPTIONS
//

if($_SESSION['validUser'] == "no"){
	//
	//LOGIN DROPDOWN
	//
		$navBarOptions = "        
		<li class='dropdown'>
          <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Login<span class='caret'></span></a>
			<form method='post' name='loginForm' action='login.php' class='dropdown-menu'>
    
    			<p>Username:</p> 
        		<input type='text' class='blackText' name='inUsername' />
        		<p>Password:</p>
        		<input type='password' class='blackText' name='inPassword' />
        		<p><input type='submit' class='blackText' name='login' value='Login' /><input type='reset' class='blackText' name='reset' /></p>
			</form>
        </li>
		
		<li><a href='help.php'>Help</a></li>
		
		";
}

else{
	$navBarOptions = "
            		<li><a href='addMovie.php'>Add Movie</a></li>
            		<li><a href='logout.php'>Logout</a></li>
					<li><a href='help.php'>Help</a></li>
					";
	}

//
//validation functions
//


$nameError = "";
$emailError = "";
$reasonError = "";
$commentsError = "";

$validForm = false;

$inName = "";
$inEmail = "";
$inReason = "";
$inComments = "";

function validateName(){
	global $nameError, $validForm, $inName;
	if($inName == ""){
		$validForm = false;
		$nameError = "Name Required.";
	}
	if(preg_match('/[0-9!@#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $inName)){
		$validForm = false;
		$nameError = "No Special Characters or Numbers.";
	}
}

function validateReason(){
	global $reasonError, $validForm, $inReason;
	if(empty($inReason)){
			$reasonError = "Please Select One.";
			$validForm = false;
	}
}

function validateEmail(){
	global $inEmail, $validForm, $emailError;
	if(!filter_var($inEmail, FILTER_VALIDATE_EMAIL)){
		$emailError = "Please enter a valid email.";
		$validForm = false;		
	}
}

function validateComments(){
	global $validForm, $commentsError, $inComments;
		if(!strlen(trim($inComments))){
			$commentsError = "Please Explain.";
			$validForm = false;
		}
}


///pulling data from form
if(isset($_POST['submit'])){
	$inName = trim($_POST['inName']);
	$inEmail = $_POST['inEmail'];
	$inReason = $_POST['inReason'];
	$inComments = $_POST['inComments'];
	
	$validForm = true;
	
	validateName();
	validateEmail();
	validateReason();
	validateComments();
	
		if ($validForm){
			
		$newEmail = new email();

		$newEmail->set_senderEmail('contact@bwtingley.net');
		$newEmail->set_toEmail($inEmail, 'bwtingley1@dmacc.edu');
		$newEmail->set_subject('Movie Collection Help');
		$newEmail->set_message(
				"Email: ". $_POST['inEmail'].
				"<br>".
				"Name: ". $_POST['inName'].
				"<br>".
				"Date Submitted: ". date("m-d-Y").
				"<br>".
				"Time Submitted: ". date("h:i").
				'<br>'.
				'<br>'.
				"Reason for contact: ". $_POST['inReason'].
				 '<br>'.
				 "Comments: ". $_POST['inComments']

				 );

		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= "From: ". $newEmail->senderEmail;
		
		$to = "bwtingley1@dmacc.edu" . ', ';
		$to .= $newEmail->toEmail;

		mail($to,$newEmail->subject,$newEmail->message,$headers);
		}

}




?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Movie Collection</title>

<!-- Latest compiled and minified CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<link href="css/styles.css" rel="stylesheet" type="text/css">

<!-- jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



</head>

<body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Movie Collection</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
                        <?php echo $navBarOptions; ?>
                                     
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <br>
    <br>
    <br>
<div class="container container-black">
<br>
	<div class="col-sm-12">
    
    
    <!--form-->
    <?php if(isset($_POST['submit']) && $validForm){
		echo "<h3 class='text-center'>You're email has been sent, thank you.</h3>";	
		echo "<p class='text-center'>This page will auto-redirect in 5 seconds. If not, click <a href='index.php'>here.</a></p>";
		header( "refresh:5;url=index.php" );
	}
	else{
 	?>
    <h2 class="text-center">Contact</h2>
 		<form name="form1" method="post" action="help.php" class="text-center">
  <p>&nbsp;</p>
  <p>
    <label>Your Name:   </label>
      <input type="text" name="inName" id="inName" value="<?php echo isset($_POST['inName']) ? $_POST['inName'] : ''; ?>" /><span class="error"><?php echo $nameError; ?></span>
    </label>
  </p>
  <p><label>Your Email: </label>
    <input type="text" name="inEmail" id="inEmail" value="<?php echo isset($_POST['inEmail']) ? $_POST['inEmail'] : ''; ?>" /><span class="error"><?php echo $emailError; ?></span>
 
  </p>
  <p> 
    <label>Reason for contact: </label>
      <select name="inReason" id="inReason">
        <option value="">Please Select a Reason</option>
        <option value="Question" <?php echo isset($_POST["inReason"]) && $_POST["inReason"] == "product" ? "selected" : "" ?>>Question</option>
        <option value="Website Problem" <?php echo isset($_POST["inReason"]) && $_POST["inReason"] == "return" ? "selected" : "" ?>>Website Problem</option>
      </select>
      <span class="error"><?php echo $reasonError; ?></span>
    
  </p>
  <p>
    <label>Comments:<br></label>
      <textarea name="inComments" id="inComments" cols="45" rows="5"></textarea>
    
    <span class="error"><?php echo $commentsError; ?></span>
  </p>
 

  <p>
    <input type="submit" name="submit" id="button" value="Submit">
    <input type="reset" name="button2" id="button2" value="Reset">
  </p>
</form>
<?php } ?>


    </div>
</div>
</body>
</html>