<?php

session_cache_limiter('none');
session_start();

if(empty($_SESSION['validUser'])){
    $_SESSION['validUser'] = "no";
}

//
//NAVBAR LOGIN//OPTIONS
//

if($_SESSION['validUser'] == "no"){
	//
	//LOGIN DROPDOWN
	//
		$navBarOptions = "        
		<li class='dropdown'>
          <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Login<span class='caret'></span></a>
			<form method='post' name='loginForm' action='login.php' class='dropdown-menu'>
    
    			<p>Username:</p> 
        		<input type='text' class='blackText' name='inUsername' />
        		<p>Password:</p>
        		<input type='password' class='blackText' name='inPassword' />
        		<p><input type='submit' class='blackText' name='login' value='Login' /><input type='reset' class='blackText' name='reset' /></p>
			</form>
        </li>
		
		<li><a href='help.php'>Help</a></li>"
		;
}

else{
	$navBarOptions = "
            		<li><a href='addMovie.php'>Add Movie</a></li>
            		<li><a href='logout.php'>Logout</a></li>
					<li><a href='help.php'>Help</a></li>
					";
	}

//
//Showing movie data
//

include 'connection.php';

$updateRecId = $_GET['recId'];

$sql = "SELECT movie_name,movie_description,movie_genre,movie_rating,movie_time FROM movie_table WHERE movie_id=?";

		$query = $connection->prepare($sql);
		
		$query->bind_param("i",$updateRecId);	
	
		if( $query->execute() )	
		{
			$query->bind_result($movie_name,$movie_description,$movie_genre,$movie_rating,$movie_time);
		
			$query->store_result();
			
			$query->fetch();
		}
		else
		{
			$message = "<h1>You have encountered a problem with your update.</h1>";
			$message .= "<h2>" . mysqli_error($connection) . "</h2>" ;			
		}
		
$connection->close();



?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Movie Collection</title>

<!-- Latest compiled and minified CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<link href="css/styles.css" rel="stylesheet" type="text/css">

<!-- jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


</head>

<body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Movie Collection</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
                        <?php echo $navBarOptions; ?>
                                     
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <br>
    <br>
    <br>
<div class="container container-black">
<br>
	<div class="col-sm-12">
		<strong>Title:</strong><p><?php echo $movie_name; ?></p>
        <strong>Genre:</strong><p><?php echo $movie_genre; ?></p>
        <strong>Time:</strong><p><?php echo $movie_time; ?></p>
        <strong>Rating:</strong><p><?php echo $movie_rating; ?></p>
        <strong>Description:</strong><p><?php echo $movie_description; ?></p>
    </div>
</div>
</body>
</html>